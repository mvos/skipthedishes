resource "aws_instance" "jenkins_master" {
    ami = "ami-5339733f"
    instance_type = "t2.micro"
    key_name = "${aws_key_pair.jenkins.key_name}"
    subnet_id = "${aws_subnet.main-public.id}"
    vpc_security_group_ids = ["${aws_security_group.ssh_and_http.id}"]
    tags {
        Name = "jenkins_master"
        role = "jenkins_master"
    }
    
    provisioner "file" {
    source      = "scripts/setup_jenkins.sh"
    destination = "/tmp/setup_jenkins.sh"
  }
    provisioner "file" {
    source      = "scripts/jenkins_nginx.conf"
    destination = "/tmp/jenkins.conf"
  }
    provisioner "remote-exec" {
    inline = [
      "sudo chmod +x /tmp/setup_jenkins.sh",
      "bash /tmp/setup_jenkins.sh",
    ]
  }
  connection {
    user = "${var.INSTANCE_USERNAME}"
    private_key = "${file("${var.PATH_TO_PRIVATE_KEY}")}"
  }
}
resource "aws_instance" "php_master" {
    ami = "ami-5339733f"
    instance_type = "t2.micro"
    key_name = "${aws_key_pair.jenkins.key_name}"
    subnet_id = "${aws_subnet.main-public.id}"
    vpc_security_group_ids = ["${aws_security_group.ssh_and_http.id}"]
    tags {
        Name = "php_master"
        role = "php_master"
    }
    provisioner "file" {
    source      = "scripts/setup_php.sh"
    destination = "/tmp/setup_php.sh"
  }
    provisioner "file" {
    source      = "scripts/php_nginx.conf"
    destination = "/tmp/php.conf"
  }
    provisioner "file" {
    source      = "scripts/php-fpm.conf"
    destination = "/tmp/php-fpm.conf"
  }
    provisioner "remote-exec" {
    inline = [
      "sudo chmod +x /tmp/setup_php.sh",
      "bash /tmp/setup_php.sh",
    ]
  }
connection {
    user = "${var.INSTANCE_USERNAME}"
    private_key = "${file("${var.PATH_TO_PRIVATE_KEY}")}"
  }
}
