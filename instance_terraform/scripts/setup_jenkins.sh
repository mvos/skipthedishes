sudo yum update -y
sudo wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat/jenkins.repo
sudo rpm --import http://pkg.jenkins-ci.org/redhat/jenkins-ci.org.key
sudo yum install jenkins docker nginx git java-1.8.0-openjdk -y
sudo yum remove java-1.7.0-openjdk java-1.7.0-openjdk-headless -y
sudo cp /tmp/jenkins.conf /etc/nginx/conf.d/
sudo sed -i 's/listen       80 default_server/listen       80/g' /etc/nginx/nginx.conf
sudo sed -i 's/listen       [::]:80 default_server;listen       [::]:80/g' /etc/nginx/nginx.conf
sudo usermod -a -G docker jenkins
sudo usermod -a -G docker ec2-user
sudo service docker start
sudo service jenkins start
sudo service nginx start
sudo chkconfig jenkins on
sudo chkconfig nginx on
sudo chkconfig docker on
cd /tmp
sudo wget http://localhost:8080/jnlpJars/jenkins-cli.jar
sudo java -jar jenkins-cli.jar -s http://localhost:8080 install-plugin checkstyle cloverphp crap4j dry htmlpublisher jdepend plot pmd violations warnings xunit git slack build-monitor-pluginjava -jar jenkins-cli.jar -s http://localhost:8080 install-plugin checkstyle cloverphp crap4j dry htmlpublisher jdepend plot pmd violations warnings xunit git slack build-monitor-plugin
sudo java -jar jenkins-cli.jar -s http://localhost:8080 safe-restart
