sudo yum -y update
sudo yum -y install php55 php55-fpm php55-mysqlnd php55-odbc nginx git
sudo mkdir -p /var/log/nginx
sudo touch /var/log/nginx/access.log
sudo mkdir -p /run/nginx
sudo mkdir /etc/php5
sudo rm -Rf /etc/php5/php-fpm.conf
sudo cp /tmp/php.conf /etc/nginx/conf.d/
sudo cp /tmp/php-fpm.conf /etc/php5/
sudo sed -i 's/listen       80 default_server/listen       80/g' /etc/nginx/nginx.conf
sudo sed -i 's/listen       \[::\]:80 default_server/listen       \[::\]:80/g' /etc/nginx/nginx.conf
cd /tmp
sudo git clone https://bitbucket.org/mvos/php_mysql_simple_cart.git
sudo mkdir /www
sudo mv /tmp/php_mysql_simple_cart/* /www/
sudo service php-fpm restart
sudo service nginx restart
sudo chkconfig php-fpm on
sudo chkconfig nginx on
