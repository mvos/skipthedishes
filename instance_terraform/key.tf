resource "aws_key_pair" "jenkins" {
  key_name = "jenkins"
  public_key = "${file("${var.PATH_TO_PUBLIC_KEY}")}"
}
