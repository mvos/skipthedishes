variable "AWS_REGION" {
  default = "sa-east-1"
}
variable "PATH_TO_PRIVATE_KEY" {
  default = "jenkins"
}
variable "PATH_TO_PUBLIC_KEY" {
  default = "jenkins.pub"
}
variable "INSTANCE_USERNAME" {
  default = "ec2-user"
}
variable "RDS_PASSWORD" {
  default = "3c0mPHP!"
}
