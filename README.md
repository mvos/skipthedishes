## SkipTheDishes DevOps project - Marcus Vinicius Oliveira Silva - mvostt@gmail.com ##

# Built with Terraform on AWS: #

Servers, RDS, VPC, Keypair, Security Group

# Jenkins server #

- http://ec2-54-233-129-109.sa-east-1.compute.amazonaws.com

# User - PW - Email - EmailPW
- developer - dev - mvosskipdevops.dev@gmail.com - devopsskip

- production - prd - mvosskipdevops.prd@gmail.com - devopsskip

# APP PHP Ecom server #

- http://ec2-54-233-84-173.sa-east-1.compute.amazonaws.com

- user - test@test.com

- pw - test

# APP git repo #

- https://bitbucket.org/mvos/php_mysql_simple_cart/src

- ( access through the dev and prd users )

# References:#
- https://www.terraform.io/docs/index.html
- https://jenkins.io/doc/
- http://codingcyber.org/simple-shopping-cart-application-php-mysql-6394/
